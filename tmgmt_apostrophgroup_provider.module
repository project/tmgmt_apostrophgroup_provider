<?php

/**
 * @file
 * Module file of the Apostroph Translator.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\tmgmt_apostrophgroup_provider\Util\AXliff;
use Drupal\file\Entity\File;
use Drupal\tmgmt_apostrophgroup_provider\Util\GeneralHelper;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt_apostrophgroup_provider\Restclient\Configuration;
use Drupal\tmgmt_apostrophgroup_provider\Restclient\Api\TranslationApi;
use Drupal\tmgmt_apostrophgroup_provider\Restclient\Model\OrderState;
use Drupal\tmgmt\Entity\Job;
use Drupal\Core\File\FileSystemInterface;

/**
 * Import form submit callback.
 */
function tmgmt_apostrophgroup_provider_import_form_submit(array $form, FormStateInterface $form_state)
{
  // Ensure we have the file uploaded.
  $job = $form_state->getFormObject()->getEntity();
  $supported_formats = array_keys(Drupal::service('plugin.manager.tmgmt_file.format')->getDefinitions());
  array_push($supported_formats,'xliff');
  if ($file = file_save_upload('file', array('file_validate_extensions' => array(implode(' ', $supported_formats))), FALSE, 0)) {
    $extension = pathinfo($file->getFileUri(), PATHINFO_EXTENSION);
    $plugin = new AXliff(array(), 'xlf', NULL);
    if ($plugin) {
      // Validate the file on job.
      $validated_job = $plugin->validateImport($file->getFileUri(), $job);
      if (!$validated_job) {
        $job->addMessage('Failed to validate file, import aborted.', array(), 'error');
      } elseif ($validated_job->id() != $job->id()) {
        $job->addMessage('The imported file job id @file_id does not match the job id @job_id.', array(
          '@file_id' => $validated_job->id(),
          '@job_id' => $job->id(),
        ), 'error');
      } else {
        try {
          // Validation successful, start import.
          GeneralHelper::resetJobandItemsToActive($job, $file);
          $job->addTranslatedData($plugin->import($file->getFileUri()));
          $job->addMessage('Successfully imported file.');
        } catch (Exception $e) {
          $job->addMessage('File import failed with the following message: @message', array('@message' => 'for further info please check Drupal log.'), 'error');
          \Drupal::logger('TMGMT_APOSTROPH')->error('File import failed with the following message:: %message ', [
            '%message' => $e->getMessage(),
          ]);
        }
      }
    }
  }
  tmgmt_write_request_messages($job);
}

/**
 * Download jobs using cron.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function tmgmt_apostrophgroup_provider_cron()
{
  // Get all translators.
  $translators = \Drupal::service('entity_type.manager')->getStorage('tmgmt_translator')->loadByProperties(['plugin' => 'tmgmt_apostrophgroup_provider']);

  if (!$translators) {
    return;
  }


  foreach ($translators as $translator) {
    $config = $translator->getSetting('cron-settings');

    // Make sure cron is enabled for this translator.
    if (!$config['status']) {
      \Drupal::logger('TMGMT_APOSTROPH')->notice('Cron disabled for translator %translator', [
        '%translator' => $translator->label(),
      ]);
      continue;
    }
    tmgmt_apostrophgroup_provider_downlaod_data_by_translator($translator);
  }
}

/**
 * Downlaod data by translator.
 *
 * @param \Drupal\tmgmt\TranslatorInterface $translator
 *   Tranlator.
 */
function tmgmt_apostrophgroup_provider_downlaod_data_by_translator(TranslatorInterface $translator, $only_finished_remote_jobs = TRUE)
{
  try {
    $all_local_jobs = GeneralHelper::getAllJobsByTranslator($translator, TRUE);
    $apostrophsettings = $translator->getSetting('apostroph-settings');
    $config = new Configuration();
    $config->setHost($apostrophsettings['url']);
    $config->setUsername($apostrophsettings['username']);
    $config->setPassword($apostrophsettings['password']);
    $translation_api = new TranslationApi(NULL, $config);
    $all_remote_status = $translation_api->translations();
    foreach ($all_local_jobs as $job) {
      $job_found_remote = FALSE; //array_search($job->getReference(), $all_remote_status);
      foreach ($all_remote_status as $remote_state) {
        if ($job->getReference() == $remote_state->getTranslationId()) {
          $job_found_remote = TRUE;
          if ($only_finished_remote_jobs && $remote_state->getState() != OrderState::FINISHED) {
            $job_found_remote = FALSE;
          }
          break;
        }
      }
      if ($job->getState() == Job::STATE_ACTIVE && $job_found_remote) {
        tmgmt_apostrophgroup_provider_downlaod_data_by_job($translator, $job);
      }
    }
  } catch (Exception $exception) {
    \Drupal::logger('TMGMT_APOSTROPH')->error('Could not check delivery list for provider %provider: %message', [
      '%provider' => $translator->id(),
      '%message' => $exception->getMessage(),
    ]);
    \Drupal::messenger()->addError('An error occured while fetching and importing files: ' . $exception->getMessage());
  }
}

/**
 * Implements hook_tmgmt_job_delete().
 */
function tmgmt_apostrophgroup_provider_tmgmt_job_delete(JobInterface $job)
{
  // Ignore jobs that don't have a file translator.
  if (!$job->hasTranslator() || $job->getTranslator()->getPluginId() != 'tmgmt_apostrophgroup_provider') {
    return;
  }

  if ($job->getState() == Job::STATE_FINISHED) {
    return;
  }
  try {
    $translator = $job->getTranslator();
    $apostrophsettings = $translator->getSetting('apostroph-settings');
    $config = new Configuration();
    $config->setHost($apostrophsettings['url']);
    $config->setUsername($apostrophsettings['username']);
    $config->setPassword($apostrophsettings['password']);
    $translation_api = new TranslationApi(NULL, $config);
    $translation_api->cancelTranslationWithHttpInfo($job->getReference());
    $job->addMessage(t("Apostroph Job cancelled:") . $job->getReference());
    \Drupal::messenger()->addError(t("Apostroph Job cancelled:") . $job->getReference());
    // Check if there are any files that need to be deleted.
    // @todo There doesn't seem to be an API function for this...
    $args = [
      ':module' => 'tmgmt_apostrophgroup_provider',
      ':type' => 'tmgmt_job',
      ':id' => $job->id(),
    ];
    $result = \Drupal::database()->query('SELECT fid FROM {file_usage} WHERE module = :module and type = :type and id = :id', $args);
    $fids = $result->fetchCol();
    if (!empty($fids)) {
      // Remove file usage record.
      /** @var \Drupal\file\FileUsage\FileUsageInterface $file_usage */
      $file_usage = \Drupal::service('file.usage');
      foreach (File::loadMultiple($fids) as $file) {
        $file_usage->delete($file, 'tmgmt_file', 'tmgmt_job', $job->id());
        // If this was the last usage, FileUsageBase marks the file as temporary
        // for delayed deletion. Because we know it is not needed, delete the file
        // immediately.
        $usage = $file_usage->listUsage($file);
        if (empty($usage)) {
          $file->delete();
        }
      }
    }
    return TRUE;
  } catch (\Exception $exception) {
    \Drupal::logger('TMGMT_APOSTROPH')->error('Error occured while deleting Apostroph job, please contact the responsible project manager: %message', [
      '%message' => $exception->getMessage(),
    ]);
    $job->addMessage(t('Error occured while deleting Apostroph job, please contact the responsible project manager: '), null, 'error');
    return TRUE;
  }
}

/**
 * Method to receive content automatically.
 *
 * @param array $form
 *   Form with input data.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   Form_state.
 */
function tmgmt_apostrophgroup_provider_semi_import_form_submit(array $form, FormStateInterface $form_state)
{
  $job = $form_state->getFormObject()->getEntity();
  $translator = $job->getTranslator();
  if ($translator) {
    try {
      tmgmt_apostrophgroup_provider_downlaod_data_by_job($translator, $job);
    } catch (Exception $exception) {
      \Drupal::logger('TMGMT_APOSTROPH')->error('Files could not be downlaoded or imported, see error message:  %message', [
        '%message' => $exception->getMessage(),
      ]);
      $job->addMessage('Files could not be downlaoded or imported, see error message in Drupal log. ');
    }
  }
  tmgmt_write_request_messages($job);
}

/**
 * Method to downlaod data.
 *
 * @param \Drupal\tmgmt\TranslatorInterface $translator
 *   Translator.
 * @param \Drupal\tmgmt\JobInterface $job
 *   Job.
 */
function tmgmt_apostrophgroup_provider_downlaod_data_by_job(TranslatorInterface $translator, JobInterface $job) {
  try {
    $prId = $job->getReference();
    $apostrophsettings = $translator->getSetting('apostroph-settings');
    \Drupal::logger('tmgmt_apostrophgroup_provider')->debug('Starting data download for job ID: @job_id, Reference ID: @prId', [
      '@job_id' => $job->id(),
      '@prId' => $prId,
    ]);

    // Configuring the API
    $config = new Configuration();
    $config->setHost($apostrophsettings['url']);
    $config->setPassword($apostrophsettings['password']);
    $config->setUsername($apostrophsettings['username']);
    $translation_api = new TranslationApi(NULL, $config);

    // Making API call to fetch the translation by ID
    $translation_response = $translation_api->translationbyIDWithHttpInfo($prId);
    \Drupal::logger('tmgmt_apostrophgroup_provider')->debug('Received response with status code: @status', ['@status' => $translation_response[1]]);

    // Check the response
    if ($translation_response[1] == '204') {
      \Drupal::messenger()->addStatus(t('No deliveries available for job: ') . $job->label());
      \Drupal::logger('tmgmt_apostrophgroup_provider')->info('No deliveries available for job: @job_label', ['@job_label' => $job->label()]);
      return;
    }
    if ($translation_response[1] == '200') {
      $base64_content = $translation_response[0];
      $binary_content = base64_decode($base64_content);
      if ($binary_content === false) {
        \Drupal::logger('tmgmt_apostrophgroup_provider')->error('Failed to decode base64 content for job ID: @job_id', ['@job_id' => $job->id()]);
        return;
      }

      $dir = $job->getSetting('scheme') . '://tmgmt_apostrophgroup/ApostrophReceivedFiles/';
      $filesystem = \Drupal::service('file_system');
      if ($filesystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY)) {
        $joblabel = GeneralHelper::getJobLabelNoSpeChars($job);
        $filename_gz = 'gzip_job_' . $joblabel . '_' . $job->id() . "_" . $job->getRemoteSourceLanguage() . "_" . $job->getRemoteTargetLanguage() . '.gz';
        \Drupal::logger('tmgmt_apostrophgroup_provider')->debug('Directory created, starting import of file: @filename', ['@filename' => $filename_gz]);

        tmgmt_apostropgroup_provider_import($job, $binary_content, $filename_gz);
      } else {
        \Drupal::messenger()->addStatus(t('Directory could not be created: ') . $dir);
        \Drupal::logger('tmgmt_apostrophgroup_provider')->error('Failed to create directory: @dir', ['@dir' => $dir]);
        return;
      }
    }
  } catch (Exception $exception) {
    $respbody = $exception->getMessage();
    \Drupal::logger('tmgmt_apostrophgroup_provider')->error('An error occurred while fetching and importing files for job ID: @job_id. Error: @error', [
      '@job_id' => $job->id(),
      '@error' => $respbody,
    ]);
    \Drupal::messenger()->addError('An error occurred while fetching and importing files: ' . $respbody);
  }
}


/**
 * Import file into Drupal.
 *
 * @param \Drupal\tmgmt\JobInterface $job
 *   Job.
 * @param string $filedatastring
 *   File content.
 * @param string $filename
 *   Name of the file to be imported.
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function tmgmt_apostropgroup_provider_import(JobInterface $job, $filedatastring, $filename) {
  $path_gz = $job->getSetting('scheme') . '://tmgmt_apostrophgroup/ApostrophReceivedFiles/' . $filename;
  $dirname = dirname($path_gz);

  \Drupal::logger('tmgmt_apostrophgroup_provider')->debug('Preparing directory for import: @dirname', ['@dirname' => $dirname]);

  if (\Drupal::service('file_system')->prepareDirectory($dirname, FileSystemInterface::CREATE_DIRECTORY)) {
    $file_gz = \Drupal::service('file.repository')->writeData($filedatastring, $path_gz, FileSystemInterface::EXISTS_REPLACE);
    if (!$file_gz) {
      \Drupal::logger('tmgmt_apostrophgroup_provider')->error('Failed to write file data to @path', ['@path' => $path_gz]);
      return;
    }

    $pathtoextracted_file = $job->getSetting('scheme') . '://tmgmt_apostrophgroup/ApostrophReceivedFiles/' . pathinfo($path_gz, PATHINFO_FILENAME) . '.xlf';
    \Drupal::logger('tmgmt_apostrophgroup_provider')->debug('Extracting gzip file to: @path', ['@path' => $pathtoextracted_file]);

    extact_gzip_file(\Drupal::service('file_system')->realpath($file_gz->getFileUri()), \Drupal::service('file_system')->realpath($pathtoextracted_file));
    $file_gz->delete();

    $exported_xlf = GeneralHelper::createFileObject($pathtoextracted_file);
    $filearray = [$exported_xlf];

    foreach ($filearray as $fileitem) {
      $plugin = new AXliff(array(), 'xlf', NULL);
      if ($plugin) {
        $validated_job = $plugin->validateImport($fileitem->getFileUri(), $job);

        if (!$validated_job) {
          \Drupal::logger('TMGMT_APOSTROPH')->error('Failed to validate file @filename for job @job_id, import aborted.', [
            '@filename' => $fileitem->getFileName(),
            '@job_id' => $job->label(),
          ]);
          $job->addMessage('Failed to validate file, import aborted.', [], 'error');
        } elseif ($validated_job->id() != $job->id()) {
          \Drupal::logger('TMGMT_APOSTROPH')->notice('Mismatch between imported file job ID (@file_id) and actual job ID (@job_id)', [
            '@file_id' => $validated_job->id(),
            '@job_id' => $job->id(),
          ]);
          $job->addMessage('The imported file job id @file_id does not match the job id @job_id.', [
            '@file_id' => $validated_job->id(),
            '@job_id' => $job->id(),
          ], 'error');
        } else {
          try {
            GeneralHelper::resetJobandItemsToActive($job, $fileitem);
            $data = $plugin->import($fileitem->getFileUri());
            $job->addTranslatedData($data);
            \Drupal::logger('TMGMT_APOSTROPH')->notice('Successfully imported file @file_id for job @job_id.', [
              '@file_id' => $fileitem->getFileName(),
              '@job_id' => $job->label(),
            ]);
            $job->addMessage('File @filename imported successfully', ['@filename' => $fileitem->getFileName()]);
          } catch (Exception $e) {
            \Drupal::logger('TMGMT_APOSTROPH')->error('File import failed for job @job_id with message: @message', [
              '@job_id' => $job->label(),
              '@message' => $e->getMessage(),
            ]);
            $job->addMessage('File import failed with the following message: @message', [
              '@message' => 'for further info please check Drupal log.'
            ], 'error');
          }
        }
      }
    }
  } else {
    \Drupal::logger('tmgmt_apostrophgroup_provider')->error('Failed to create directory for path @dirname', ['@dirname' => $dirname]);
  }
}



function extact_gzip_file($gzip_file, $destination)
{
  $gz = gzopen($gzip_file, 'rb');
  if (!$gz) {
    throw new \UnexpectedValueException(
      'Could not open gzip file'
    );
  }

  $dest = fopen($destination, 'wb');
  if (!$dest) {
    gzclose($gz);
    throw new \UnexpectedValueException(
      'Could not open destination file'
    );
  }

  // transfer ...
  stream_copy_to_stream($gz, $dest);
  gzclose($gz);
  fclose($dest);
}

/**
 * Create fiel object from StdClass.
 *
 * @param array $filearray
 *   array with stClass.
 *
 * @return array
 *   Array with file object.
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function apcreate_file_object_from_stdclass(array $filearray)
{
  $filsystem = \Drupal::service('file_system');
  $toreturn = array();
  foreach ($filearray as $fl) {
    $file = File::create([
      'uid' => \Drupal::currentUser()->id(),
      'filename' => $filsystem->basename($fl->filename),
      'uri' => $fl->uri,
      'filemime' => \Drupal::service('file.mime_type.guesser')->guess($fl->uri),
      'filesize' => filesize($fl->uri),
      'status' => 1
    ]);
    $file->save();
    array_push($toreturn, $file);
  }
  return $toreturn;
}

function tmgmt_apostrophgroup_provider_check_empty_file($form, &$form_state)
{
  $all_files = \Drupal::request()->files->get('files', []);
  // Make sure there's an upload to process.
  if (empty($all_files['file'])) {
    $form_state->setErrorByName('file', t('No files selected, action cannot be completed.'));
  }
}
