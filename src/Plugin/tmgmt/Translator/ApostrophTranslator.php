<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/7/2020
 * Time: 2:25 PM
 */

namespace Drupal\tmgmt_apostrophgroup_provider\Plugin\tmgmt\Translator;

use Drupal\Component\Utility\Xss;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\Entity\JobItem;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\Translator\TranslatableResult;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\TranslatorPluginBase;
use Drupal\tmgmt_apostrophgroup_provider\Restclient\Configuration;
use Drupal\tmgmt_apostrophgroup_provider\Restclient\Api\TranslationApi;
use Drupal\tmgmt_apostrophgroup_provider\Restclient\Model\TranslationData;
use Drupal\tmgmt_apostrophgroup_provider\Util\GeneralHelper;
use ZipArchive;
use Drupal\tmgmt_apostrophgroup_provider\Util\AXliff;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Render\Markup;

/**
 * Apostroph Translator.
 *
 * @TranslatorPlugin(
 *   id = "tmgmt_apostrophgroup_provider",
 *   label = @Translation("Apostroph Group Connector"),
 *   logo = "icons/apostroph-logo.svg",
 *   description = @Translation("Provider to send content to Apostroph Group"),
 *   ui = "Drupal\tmgmt_apostrophgroup_provider\ApostrophTranslatorUI"
 * )
 */
class ApostrophTranslator extends TranslatorPluginBase
{

  /**
   * {@inheritdoc}
   */
  public function checkTranslatable(TranslatorInterface $translator, JobInterface $job)
  {
    // Anything can be exported.
    return TranslatableResult::yes();
  }

  /**
   * Submits the translation request and sends it to the translation provider.
   *
   * During the translation process, Job::getItems() will only return job items
   * that are not already fully translated.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   The job that should be submitted.
   *
   * @see hook_tmgmt_job_before_request_translation()
   * @see hook_tmgmt_job_after_request_translation()
   *
   * @ingroup tmgmt_remote_languages_mapping
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function requestTranslation(JobInterface $job) {
    try {
        $job_id = $job->id();
        $job_label = GeneralHelper::getJobLabelNoSpeChars($job);
        
        \Drupal::logger('tmgmt_apostrophgroup_provider')->debug('Starting requestTranslation for job ID: @job_id, Label: @job_label', [
            '@job_id' => $job_id,
            '@job_label' => $job_label,
        ]);

        $translator = $job->getTranslator();
        $apostrophsettings = $translator->getSetting("apostroph-settings");
        $oneexportfile = $translator->getSetting("one_export_file");
        $exporter = new AXliff([], "xlf", NULL);

        $dirnameallfiles = "{$job_label}_{$job_id}_{$job->getRemoteSourceLanguage()}_{$job->getRemoteTargetLanguage()}";
        $zipName = "zip_job_{$dirnameallfiles}.zip";
        $allfilespath = $job->getSetting('scheme') . '://tmgmt_apostrophgroup/ApostrophSentFiles/' . $dirnameallfiles;
        $zipPath = $allfilespath . "/" . $zipName;

        // Ensure the directory exists for storing the ZIP file
        $filesystem = \Drupal::service('file_system');
        if (!$filesystem->prepareDirectory($allfilespath, FileSystemInterface::CREATE_DIRECTORY)) {
            \Drupal::logger('tmgmt_apostrophgroup_provider')->error('Could not create directory for export: @path', ['@path' => $allfilespath]);
            throw new \Exception("Could not create directory for export: " . $allfilespath);
        }

        \Drupal::logger('tmgmt_apostrophgroup_provider')->debug('Directory created: @path', ['@path' => $allfilespath]);

        // Create the ZIP archive and add files directly to it
        $ziparchive = new ZipArchive();
        if ($ziparchive->open($filesystem->realpath($zipPath), ZipArchive::CREATE | ZipArchive::OVERWRITE)) {
            if ($oneexportfile) {
                // Export all items into one file and add to ZIP
                $name = "{$job_label}_{$job_id}_all_{$job->getRemoteSourceLanguage()}_{$job->getRemoteTargetLanguage()}.xlf";
                \Drupal::logger('tmgmt_apostrophgroup_provider')->debug('Adding all job items to one file in ZIP: @filename', ['@filename' => $name]);
                $ziparchive->addFromString($name, $exporter->export($job));
            } else {
                // Export each item separately and add each to ZIP
                foreach ($job->getItems() as $item) {
                    $labelname = GeneralHelper::getStringNoSpeChars($item->label());
                    $name = "{$labelname}_{$job_id}_{$item->id()}_{$job->getRemoteSourceLanguage()}_{$job->getRemoteTargetLanguage()}.xlf";
                    \Drupal::logger('tmgmt_apostrophgroup_provider')->debug('Adding item to ZIP: @filename', ['@filename' => $name]);
                    $ziparchive->addFromString($name, $exporter->exportItem($item));
                }
            }
            $ziparchive->close();

            // Register ZIP file with Drupal
            \Drupal::logger('tmgmt_apostrophgroup_provider')->debug('ZIP file created successfully at @zipPath', ['@zipPath' => $zipPath]);
            $zipfileobj = GeneralHelper::createFileObject($zipPath);
            \Drupal::service('file.usage')->add($zipfileobj, 'tmgmt_apostrophgroup_provider', 'tmgmt_job', $job->id());

            // Send ZIP file to Apostroph
            $remote_job_id = $this->sendZipToApostroph($apostrophsettings, $job, $zipPath);

            // Set job reference and mark as submitted
            $job->set('reference', $remote_job_id);
            $job->submitted("Job sent to provider!");
            \Drupal::logger('tmgmt_apostrophgroup_provider')->info('Job successfully sent to Apostroph with remote job ID: @remote_job_id', ['@remote_job_id' => $remote_job_id]);

            // Display download link message
            $messageTopass = 'Exported files can be downloaded here: <br/>';
            $messageTopass .= '<a href="' . \Drupal::service('file_url_generator')->generateAbsoluteString($zipPath) . '">' . Xss::filter($job_label) . '</a>';
            \Drupal::messenger()->addMessage(Markup::create($messageTopass));

        } else {
            \Drupal::logger('tmgmt_apostrophgroup_provider')->error('Could not create ZIP archive at path: @zipPath', ['@zipPath' => $zipPath]);
            throw new \Exception("Could not create ZIP archive at path: " . $zipPath);
        }

    } catch (\Exception $exception) {
        $this->handleCleanupAndError($job, $zipPath, $exception);
        \Drupal::logger('tmgmt_apostrophgroup_provider')->error('Exception in requestTranslation: @message', ['@message' => $exception->getMessage()]);
    }
}

/**
 * Sends the ZIP file to the external Apostroph service with retry logic.
 */
private function sendZipToApostroph($apostrophsettings, JobInterface $job, $zipPath) {
    $job_id = $job->id();
    \Drupal::logger('tmgmt_apostrophgroup_provider')->debug('Preparing to send ZIP to Apostroph for job ID: @job_id', ['@job_id' => $job_id]);
    
    $config = new Configuration();
    $config->setHost($apostrophsettings['url']);
    $config->setUsername($apostrophsettings['username']);
    $config->setPassword($apostrophsettings['password']);
    $translation_api = new TranslationApi(NULL, $config);
    $translation_data = GeneralHelper::createTranslationRequest($apostrophsettings, $job, $zipPath);
    $translation_request = new TranslationData($translation_data);

    // Log the request payload to help debug any issues with the data being sent
    \Drupal::logger('tmgmt_apostrophgroup_provider')->debug('Payload sent to Apostroph: @payload', ['@payload' => json_encode($translation_data)]);

    $retries = 3;
    while ($retries > 0) {
        try {
            $response = $translation_api->createnewTranslation($translation_request);
            \Drupal::logger('tmgmt_apostrophgroup_provider')->info('Apostroph response received for job ID: @job_id', ['@job_id' => $job_id]);
            return $response->getTranslationId();
        } catch (\Exception $ex) {
            $retries--;
            \Drupal::logger('tmgmt_apostrophgroup_provider')->warning('Retrying send to Apostroph. Attempts left: @retries', ['@retries' => $retries]);
            
            // Log full response headers if the exception provides it
            if (method_exists($ex, 'getResponse') && $ex->getResponse()) {
                \Drupal::logger('tmgmt_apostrophgroup_provider')->debug('Response headers: @headers', ['@headers' => json_encode($ex->getResponse()->getHeaders())]);
            }
            
            if ($retries === 0) {
                throw new \Exception("Failed to send ZIP to Apostroph after multiple attempts: " . $ex->getMessage());
            }
        }
    }
}

/**
 * Handles cleanup and error messaging in case of failure.
 */
private function handleCleanupAndError(JobInterface $job, $zipPath, \Exception $exception) {
    \Drupal::logger('tmgmt_apostrophgroup_provider')->error('Cleaning up after exception: @message', ['@message' => $exception->getMessage()]);
    
    $job->setState(Job::STATE_UNPROCESSED);

    // Attempt to delete ZIP file if it was created
    $filesystem = \Drupal::service('file_system');
    if (file_exists($filesystem->realpath($zipPath))) {
        $filesystem->delete($filesystem->realpath($zipPath));
        \Drupal::logger('tmgmt_apostrophgroup_provider')->debug('Deleted ZIP file: @zipPath', ['@zipPath' => $zipPath]);
    }

    // Attempt to cancel job on Apostroph if necessary (if partially created)
    try {
        // TODO: Implement API call to cancel the remote job if partially created.
        \Drupal::logger('tmgmt_apostrophgroup_provider')->debug('Attempted to cancel remote job on Apostroph platform');
    } catch (\Exception $ex) {
        \Drupal::messenger()->addError("Failed to delete job on Apostroph platform: " . $ex->getMessage());
    }

    // Clean up any temporary directories
    $allfilespath = dirname($zipPath);
    if (file_exists($filesystem->realpath($allfilespath))) {
        $filesystem->deleteRecursive($allfilespath);
        \Drupal::logger('tmgmt_apostrophgroup_provider')->debug('Deleted directory: @path', ['@path' => $allfilespath]);
    }

    \Drupal::messenger()->addError($exception->getMessage());
}


  /**
   * {@inheritdoc}
   */
  public function abortTranslation(JobInterface $job)
  {
    // Assume that we can abort a translation job at any time.
    if (!$job->hasTranslator() || $job->getTranslator()->getPluginId() != 'tmgmt_apostrophgroup_provider') {
      return;
    }
    try {
      $translator = $job->getTranslator();
      $apostrophsettings = $translator->getSetting('apostroph-settings');
      $config = new Configuration();
      $config->setHost($apostrophsettings['url']);
      $config->setUsername($apostrophsettings['username']);
      $config->setPassword($apostrophsettings['password']);
      $translation_api = new TranslationApi(NULL, $config);
      $response = $translation_api->cancelTranslationWithHttpInfo($job->getReference());
      $job->aborted();
      $job->addMessage(t("Apostroph Job cancelled:") . $job->getReference());
      \Drupal::messenger()->addStatus(t("Apostroph Job cancelled:") . $job->getReference());
      return TRUE;
    } catch (\Exception $exception) {
      \Drupal::logger('TMGMT_APOSTROPH')->error('Error occured while deleting Apostroph job, please contact the responsible project manager: %message', [
        '%message' => $exception->getMessage(),
      ]);
      $job->addMessage(t('Error occured while deleting Apostroph job, please contact the responsible project manager.'), null, 'error');
      $job->aborted();
      return TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultSettings()
  {
    return array(
      'export_format' => 'xlf',
      'scheme' => 'public',
      // Making this setting TRUE by default is more appropriate, however we
      // need to make it FALSE due to backwards compatibility.
      'xliff_processing' => FALSE,
      'xliff_cdata' => FALSE,
      'username' => '',
      'password' => '',
      'one_export_file' => TRUE,
      'customer_id' => '',
      'is_confidential' => FALSE
    );
  }
}
