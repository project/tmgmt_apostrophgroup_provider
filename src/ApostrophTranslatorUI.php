<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/7/2020
 * Time: 3:29 PM
 */

namespace Drupal\tmgmt_apostrophgroup_provider;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tmgmt_apostrophgroup_provider\Restclient\Configuration;
use Drupal\tmgmt_apostrophgroup_provider\Restclient\Api\ServiceStatusApi;
use Drupal\tmgmt_apostrophgroup_provider\Restclient\Api\TranslationApi;
use Drupal\Core\Render\Element\Checkbox;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt\TranslatorPluginUiBase;

/**
 * Apostroph File translator UI.
 */
class ApostrophTranslatorUI extends TranslatorPluginUiBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();
    $apostrophsettings = $translator->getSetting('apostroph-settings');
    $cronsettings = $translator->getSetting('cron-settings');

    $form['xliff_cdata'] = array(
      '#type' => 'checkbox',
      '#title' => t('XLIFF CDATA'),
      '#description' => t('Select to use CDATA for import/export.'),
      '#default_value' => $translator->getSetting('xliff_cdata'),
    );

    $form['xliff_processing'] = array(
      '#type' => 'checkbox',
      '#title' => t('Extended XLIFF processing'),
      '#description' => t('Select to further process content semantics and mask HTML tags instead of just escaping them.'),
      '#default_value' => $translator->getSetting('xliff_processing'),
    );

    $form['xliff_message'] = array(
      '#type' => 'container',
      '#markup' => t('By selecting CDATA option, XLIFF processing will be ignored.'),
      '#attributes' => array(
        'class' => array('messages messages--warning'),
      ),
    );

    $export_one_file = $translator->getSetting('one_export_file');
    $form['one_export_file'] = array(
      '#type' => 'checkbox',
      '#disabled' => TRUE,
      '#title' => t('Use one export file for all items in job'),
      '#description' => t('Select to export all items to one file. Clear to export items to multiple files.'),
      '#value' => isset($export_one_file) ? $export_one_file : TRUE,
    );

    // Any visible, writeable wrapper can potentially be used for the files
    // directory, including a remote file system that integrates with a CDN.
    foreach (\Drupal::service('stream_wrapper_manager')->getDescriptions(StreamWrapperInterface::WRITE_VISIBLE) as $scheme => $description) {
      $options[$scheme] = Html::escape($description);
    }

    if (!empty($options)) {
      $form['scheme'] = array(
        '#type' => 'radios',
        '#title' => t('Download method'),
        '#default_value' => $translator->getSetting('scheme'),
        '#options' => $options,
        '#description' => t('Choose where  to store exported files. Recommendation: Use a secure location to prevent unauthorized access.'),
      );
    }

    $form['apostroph-settings'] = array(
      '#type' => 'details',
      '#title' => t('Apostroph REST API Settings'),
      '#open' => TRUE,
    );

    $form['apostroph-settings']['is_confidential'] = array(
      '#type' => 'checkbox',
      '#title' => t('Confidential Orders'),
      '#description' => t('Select to declare your order as confidential. This default value can be changed when submitting an order.'),
      '#default_value' => $apostrophsettings['is_confidential'],
    );

    $form['apostroph-settings']['customer_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Customer ID'),
      '#required' => TRUE,
      '#description' => t('Enter your Apostroph customer id.'),
      '#default_value' => $apostrophsettings['customer_id'],
    );

    $form['apostroph-settings']['url'] = array(
      '#type' => 'textfield',
      '#title' => t('Url'),
      '#required' => TRUE,
      '#description' => t('Enter the url to Apostroph API'),
      '#default_value' => $apostrophsettings['url'],
    );

    $form['apostroph-settings']['username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#required' => TRUE,
      '#description' => t('Enter your Apostroph username.'),
      '#default_value' => $apostrophsettings['username'],
    );

    $form['apostroph-settings']['password'] = array(
      '#type' => 'password',
      '#title' => t('Password'),
      '#required' => TRUE,
      '#description' => t('Enter your Apostroph password.'),
      '#default_value' => $apostrophsettings['password'],
    );
    $form['apostroph-settings'] += parent::addConnectButton();

    $form['cron-settings'] = array(
      '#type' => 'details',
      '#title' => t('Scheduled Tasks'),
      '#description' => t('Specify settings for scheduled tasks.'),
      '#open' => TRUE,
    );

    $form['cron-settings']['status'] = array(
      '#type' => 'checkbox',
      '#title' => t('Receive translated jobs automatically.'),
      '#description' => t('Select to receive translated jobs automatically, by scheduled task. Clear to download translated jobs manually.'),
      '#default_value' => $cronsettings['status'],
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConnect(array $form, FormStateInterface $form_state) {
    // When this method is called the form already passed validation and we can
    // assume that credentials are valid.
    $this->messenger()->addStatus(t('Successfully connected!'));
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Nothing to do here by default.
    $whotriggered = $form_state->getTriggeringElement();
    $typeofTrigger = $whotriggered['#type'];
    if ($typeofTrigger != 'selct') {
      try {
        $translator = $form_state->getFormObject()->getEntity();
        $apostrophsettings = $translator->getSetting('apostroph-settings');
        $settings = $form_state->getValue('settings');
        $apostrophsettings = $settings['apostroph-settings'];
        $username = $apostrophsettings['username'];
        $password = $apostrophsettings['password'];
        $config = new Configuration();
        $config->setHost($apostrophsettings['url']);
        $config->setUsername($username);
        $config->setPassword($password);
        $status_api = new ServiceStatusApi(NULL,$config);
        $response = $status_api->status();
        //$form_state->setValue(array('settings','apostroph-settings','password'),'');
        //$translator->setSetting('apostroph-settings',$apostrophsettings);

      }
      catch (\Exception $exception) {
        \Drupal::logger('TMGMT_APOSTROPH')->error('Failed to valideate form: %message ', [
          '%message' => $exception->getMessage(),
        ]);
        $form_state->setErrorByName('settings][apostroph-settings][password', 'Please check your username and password Settings: ' . $exception->getMessage());
        $form_state->setErrorByName('settings][apostroph-settings][username');
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function checkoutSettingsForm(array $form, FormStateInterface $form_state, JobInterface $job) {
    $translator = $job->getTranslator();
    $apostrophsettings = $translator->getSetting('apostroph-settings');
    $jobcomment = $job->getSetting("comment");
    $jobcostomerid = $job->getSetting("customer_id");
    $jobduedate = $job->getSetting("due_date");
    $jobconfidential = $job->getSetting("is_confidential");

    $form['customer_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Customer ID'),
      '#required' => TRUE,
      '#description' => t('Enter your Apostroph customer id.'),
      '#default_value' => isset($jobcostomerid)&& !empty($jobcostomerid) ? $jobcostomerid : $apostrophsettings['customer_id'],
    );

    $form['comment'] = array(
      '#type' => 'textarea',
      '#title' => t('Comment'),
      '#required' => FALSE,
      '#description' => t('Please enter any comments for the job here.'),
      '#default_value' => isset($jobcomment) ? $jobcomment : '',
    );

    $form['due_date'] = array(
      '#type' => 'date',
      '#title' => t('Desired Deadline'),
      '#required' => FALSE,
      '#description' => t('Please enter the desired due date.'),
      '#default_value' => isset($jobduedate) ? $jobduedate : \date("Y-m-d"),
    );

    $form['is_confidential'] = array(
      '#type' => 'checkbox',
      '#title' => t('Job Is Confidential'),
      '#description' => t('Select if the job is confidential.'),
      '#default_value' => $apostrophsettings['is_confidential'],
    );

    return parent::checkoutSettingsForm($form, $form_state, $job);
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutInfo(JobInterface $job) {
    //TODO: get new status from remote job using api.
    $translator = $job->getTranslator();
    $apostrophsettings = $translator->getSetting('apostroph-settings');
    $config = new Configuration();
    $config->setHost($apostrophsettings['url']);
    $config->setUsername($apostrophsettings['username']);
    $config->setPassword($apostrophsettings['password']);
    $translation_api = new TranslationApi(NULL, $config);
    $all_translations = $translation_api->translations();
    $status = "n/a";
    $remote_job_id = $job->getReference();
    foreach ($all_translations as $current_translation) {
      if ($current_translation->getTranslationId() == $remote_job_id) {
        $status = $current_translation->getState();
        break;
      }
    }
    $comment = $job->getSetting('comment');
    $customer_id = $job->getSetting('customer_id');
    $due_date = $job->getSetting('due_date');
    $confidential = $job->getSetting('is_confidential');
    $this->create_apostroph_job_form(
      $form,
      $job->label(),
      $remote_job_id,
      $status,
      $comment,
      $customer_id,
      $due_date,
      $confidential
    );

    $form['import-placeholder'] = [
      '#prefix' => '<div id="fw-im-placholder">',
      '#suffix' => '</div>',
    ];

    $form['import-placeholder']['fieldset-import'] = [
      '#type' => 'fieldset',
      '#title' => t('IMPORT TRANSLATED FILE'),
      '#collapsible' => TRUE,
    ];


    $form['import-placeholder']['fieldset-import']['automatic-import'] = [
      '#type' => 'details',
      '#title' => t('Import automatically'),
      '#open' => TRUE,
    ];

    $form['import-placeholder']['fieldset-import']['automatic-import']['auto-submit'] = [
      '#type' => 'submit',
      '#value' => t('Auto-Import'),
      '#submit' => ['tmgmt_apostrophgroup_provider_semi_import_form_submit'],
    ];

    $form['import-placeholder']['fieldset-import']['manual-import'] = [
      '#type' => 'details',
      '#title' => t('Manual Import'),
      '#open' => TRUE,
    ];

    $form['import-placeholder']['fieldset-import']['manual-import']['file'] = [
      '#type' => 'file',
      '#title' => t('File'),
      '#size' => 50,
      '#description' => t('Supported formats: xlf, xliff.'),
    ];
    $form['import-placeholder']['fieldset-import']['manual-import']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Manual Import'),
      '#submit' => ['tmgmt_apostrophgroup_provider_import_form_submit'],
      '#validate' => ['tmgmt_apostrophgroup_provider_check_empty_file'],
    ];

    return $form;
  }

  public function create_apostroph_job_form(&$fieldset, $ordername, $orderid, $orderstatus, $description, $customerid, $duedate, $isconfidential){
    $fieldset['fw-table'] = array(
      '#prefix' => '<table class="views-table views-view-table cols-8"><thead>
                            <tr>
                                <th>Job Name</th>
                                <th>Job ID</th>
                                <th>Job Status</th>
                                <th>Comments</th>
                                <th>Customer ID</th>
                                <th>Due Date</th>
                                <th>Confidential</th>
                            </tr></thead>',
      '#suffix' => '</table>',
    );
    $fieldset['fw-table']['first-row'] = array(
      '#prefix' => '<tr>',
      '#suffix' => '</tr>'
    );
    $fieldset['fw-table']['first-row']['ordername'] = array(
      '#prefix' => '<td>',
      '#markup' => $ordername,
      '#suffix' => '</td>'
    );
    $fieldset['fw-table']['first-row']['id'] = array(
      '#prefix' => '<td>',
      '#markup' => $orderid,
      '#suffix' => '</td>'
    );
    $fieldset['fw-table']['first-row']['status'] = array(
      '#prefix' => '<td>',
      '#markup' => $orderstatus,
      '#suffix' => '</td>'
    );
    $fieldset['fw-table']['first-row']['comment'] = array(
      '#prefix' => '<td>',
      '#markup' => $description,
      '#suffix' => '</td>'
    );
    $fieldset['fw-table']['first-row']['customer-id'] = array(
      '#prefix' => '<td>',
      '#markup' => $customerid,
      '#suffix' => '</td>'
    );
    $fieldset['fw-table']['first-row']['due-date'] = array(
      '#prefix' => '<td>',
      '#markup' => isset($duedate) && $duedate !== NULL ? date('D, m/d/Y - H:i:s', strtotime($duedate)) : "n/a",
      '#suffix' => '</td>'
    );

    $checkbox = array(
      '#prefix' => '<td>',
      '#type' => 'checkbox',
      '#disabled' => TRUE,
      '#value' => $isconfidential,
      '#suffix' => '</td>'
    );

    $fieldset['fw-table']['first-row']['is-confidential'] = $checkbox;
  }

}