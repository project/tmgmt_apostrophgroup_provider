<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/11/2020
 * Time: 5:12 PM
 */

namespace Drupal\tmgmt_apostrophgroup_provider\Util;

use Drupal\file\FileInterface;
use Drupal\tmgmt\Entity\JobItem;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\JobInterface;
use Drupal\Component\Utility\Xss;
use Drupal\file\Entity\File;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt\TranslatorInterface;


class GeneralHelper {

  /**
   * Get label of the job.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Job.
   *
   * @return string
   *   stingyfied label.
   */
  public static function getJobLabel(JobInterface $job) {
    return isset($job->get("label")->value) ? $job->get("label")->value : $job->label()->getArguments()["@title"];
  }

  /**
   * Remove spec. characters from label.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Job.
   *
   * @return string
   *   stingyfied label.
   */
  public static function getJobLabelNoSpeChars(JobInterface $job) {
    $toreturn = GeneralHelper::getJobLabel($job);
    return GeneralHelper::getStringNoSpeChars($toreturn);
  }

  /**
   * Remove spec. characters from label.
   *
   * @param string $arg
   *   Job.
   *
   * @return string
   *   removed spec. chars string.
   */
  public static function getStringNoSpeChars($arg) {
    $toreturn = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $arg);
    // Remove any runs of periods (thanks falstro!)
    $toreturn = mb_ereg_replace("([\.]{2,})", '', $toreturn);
    $toreturn = Xss::filter($toreturn);
    return $toreturn;
  }

  /**
   * Create file object from uri.
   *
   * @param string $uri
   *   Uri.
   *
   * @return \Drupal\file\Entity\File
   *   file class object.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function createFileObject($uri) {
    $filsystem = \Drupal::service('file_system');
    $mimeTypeGuesser = \Drupal::service('file.mime_type.guesser');

    $file = File::create([
      'uid' => \Drupal::currentUser()->id(),
      'filename' => $filsystem->basename($uri),
      'uri' => $uri,
      'filemime' => $mimeTypeGuesser->guessMimeType($uri),  // Updated method here
      'filesize' => filesize($uri),
      'status' => 1
    ]);

    $file->save();
    return $file;
}

  /**
   * Get all jobs for connector config.
   *
   * @param \Drupal\tmgmt\TranslatorInterface $translator
   *   TranslatorInterface.
   * @param bool $onlyactive
   *   Only active jobs.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   storage instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getAllJobsByTranslator(TranslatorInterface $translator, $onlyactive = FALSE) {
    $trname = $translator->id();
    if ($onlyactive) {
      $trjobs = \Drupal::service('entity_type.manager')->getStorage('tmgmt_job')->loadByProperties(['translator' => $trname, 'state' => Job::STATE_ACTIVE]);
    }
    else {
      $trjobs = \Drupal::service('entity_type.manager')
        ->getStorage('tmgmt_job')
        ->loadByProperties(['translator' => $trname]);
    }
    return $trjobs;
  }

  /**
   * Reset Job and Items to state Active.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   TranslatorInterface.
   * @param \Drupal\file\FileInterface $file
   *   TranslatorInterface.
   */
  public static function resetJobandItemsToActive(JobInterface &$job, FileInterface $file) {
    $itemsToset = $job->getItems();
    $loadedxml = simplexml_load_file(\Drupal::service('file_system')->realpath($file->getFileUri()));
    $loadedxml->registerXPathNamespace('xliff', 'urn:oasis:names:tc:xliff:document:1.2');
    foreach ($itemsToset as $item) {
      // If the xlf caontains one of job's items, set state to active. [@phase-name='extraction'].
      $tjiid = $item->id();
      $groups = $loadedxml->xpath("//xliff:group[@id='" . $tjiid . "']");
      if (count($groups) == 1) {
        $item->setState(JobItemInterface::STATE_ACTIVE);
        $job->setState(JobInterface::STATE_ACTIVE);
      }
    }
  }


  /**
   * Helper function to create tranlation request array.
   *
   * @param array $translator_settings
   *   Translator's settings.
   * @param \Drupal\tmgmt\JobInterface $job
   *   Job to be sent.
   * @param \Drupal\tmgmt_apostrophgroup_provider\Util\string $path_to_file
   *   Path to zip file that contains source file.
   * @param \Drupal\tmgmt_apostrophgroup_provider\Util\string|null $path_to_meta_file
   *   Path ot meta file.
   *
   * @return array
   *   Array with data.
   */
  public static function createTranslationRequest(array $translator_settings, JobInterface $job, string $path_to_file, string $path_to_meta_file = NULL) {
    $data = [];
    $desired_deadline = $job->getSetting('due_date');
    $comment = $job->getSetting('comment');
    $is_revision = $job->getSetting('is_revision');
    $sourse_language = $job->getRemoteSourceLanguage();
    $target_language = $job->getRemoteTargetLanguage();
    $customer_id = $job->getSetting('customer_id');
    $project_title = GeneralHelper::getJobLabelNoSpeChars($job);
    $is_confidential = $job->getSetting('is_confidential');
    $source_document_content = base64_encode(file_get_contents($path_to_file));
    $metadata_document_content = isset($path_to_meta_file) ? base64_encode(file_get_contents($path_to_meta_file)) : NULL;
    $reference_translation_id = $job->getSetting('reference_translation_id');
    $data['desired_deadline'] = isset($desired_deadline) ? $desired_deadline : NULL;
    $data['comment'] = isset($comment) ? $comment : '';
    $data['is_revision'] = isset($is_revision) ? $is_revision : FALSE;
    $data['source_language'] = isset($sourse_language) ? $sourse_language : NULL;
    $data['target_language'] = isset($target_language) ? $target_language : NULL;
    $data['project_title'] = isset($project_title) ? $project_title : 'Drupal Project';
    $data['is_confidential'] = isset($is_confidential) ? boolval($is_confidential) : FALSE;
    $data['customer_id'] = isset($customer_id) ? intval($customer_id) : intval($translator_settings['customer_id']);
    $data['source_document_content'] = isset($source_document_content) ? $source_document_content : NULL;
    $data['metadata_document_content'] = isset($metadata_document_content) ? $metadata_document_content : NULL;
    $data['reference_translation_id'] = isset($reference_translation_id) ? $reference_translation_id : NULL;
    return $data;
  }

}